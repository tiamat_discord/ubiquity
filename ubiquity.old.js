var Discord = require("discord.js");
var Config = require("./config.json");

var multiline = require("multiline");
var qs = require("querystring");

var bot = new Discord.Client();

var parlourID = "113695515719368704";
var channelID = parlourID;

bot.on("ready", function() {
  console.log(
    "Connected..! \n     Ubiquity is now connected to [" +
    bot.servers
    .length +
    "] Discord app servers and [" +
    bot.channels.length +
    "] channels with [" + bot.users
    .length + "] users."); //Does not display number of users 
  //Sets the game the bot will be shown as playing, change the number for a different game
  //Game Codes: Farming Simulator 15: 326, Spore: 245, Starcraft: 251, I Wanna Be the Guy: 295, AdVenture Capitalist: 331, The Wolf Among Us: 347, 100% Orange Juice: 351, Shadowrun: 398, Tabletop Simulator: 466, Goat Simulator: 478
  bot.setPlayingGame(398);
});

//gets timestap
var date = new Date();
console.log((date) + "\n");

bot.on("message", function(msg) {
	if (msg.content[0] === "s" && msg.content[1] === "u" && msg.content[2] ==="d" && msg.content[3] === "o") { //Still breaks 'help' suffixes as well as other commands containing more than one word 
    //Check for 'sudo', checks each character seperatly
    var command = msg.content.toLowerCase()
      .split(" ")[1]; //Command = msg content to lowercase. Split prefix. Take suffix only

    if (command !== undefined) { //If command is not empty then continue. If empty then prevent a crash
      var suffix = msg.content.toLowerCase()
        .substring(command.length +
          6); //suffix is command length + prefix length and space inbetween
      var cmd = commands[command]; //process command to find match and run code
      if (cmd) {
        cmd.process(bot, msg,
          suffix);
      }
      console.log(msg.sender + 
	      " [" +
          msg.sender.username +
          "] used: " + msg.content +
          " in #" + msg.channel.name +
          " " + msg.channel +
          "\n<@" +
          date +
          ">" + " on server " + 
          "<#" + msg.channel.server + ">"); 
		  //log user ID, user name, command used, suffix, channel name and ID, followed by timestamp and what server
		  
	    if (msg.content.toLowerCase().indexOf("ubi") != -1 || msg.isMentioned(bot.user)) {
		bot.sendMessage(msg.channel, "Try 'sudo cmds'. ");
	   }
    }
  }
});
 
//User name change

bot.on("userUpdate", (newUser, oldUser) => { //log username changes
    if (newUser.username != oldUser.username) { //check for username change
		console.log("<@" + newUser.id + "> " + newUser.username + "'s name has changed to " + oldUser.username);
		for (server in bot.servers) { //check for server in list of connected servers
		    for (member in bot.servers[server].members) { //check for members
			    if(oldUser.id === bot.servers[server].members[member].id){ //check which servers they are a member of
                    bot.sendMessage(bot.servers[server].defaultChannel, "**Info: **" + newUser.username + " has changed their name to " + oldUser.username);
                }
            }
		}
	}
});


//Log user status change; online/offline, idle/not idle 
bot.on("presence", function(data, msg) {
	console.log(data.user + 
	" [" + data.user.username + 
	"] went: " + data.status + 
	" on server <#" + data.server.name + 
	">" +
	"\n" + 
	"" + date + "");
});

//Listener
// Add a listener to the "message" event, which triggers upon receiving
// any message
bot.on( "message", function( message ) {
    var fn = onBotMessage.bind(this);
    fn(message);
    
});

bot.on('error', function(e){
    console.log('e:', e);
});

function onBotMessage(message){
    
    var content = message.content.toLowerCase().trim();
    
    var response = null; 
    
    if (response === null && content.indexOf("die") !== -1) {
         response = "ಠ_ಠ";
    }
    
     if (response === null && content.indexOf("kill myself") !== -1) {
         response = "No, don't, stop...";
     } 
     
     if (response === null && content.indexOf("kms") !== -1) {
         i = Math.floor(Math.random() * 2);
         if (i == 0) {
             response = "But you have so much to live for!";
         }
         else {
             response = ": P"; 
         }
     }
    
    var i;
    if(message.channel.id !== parlourID) {    
        return;
    }
	
    if (response === null && (content.indexOf("http://") !== -1 || content.indexOf("https://") !== -1)) {
         response = "`Post `" + "#music, " + "#linkz, " + "#filez, etc... in the appropriate channels, and keep the conversation in the #parlour!'" + "\n" + "'If you're new here, be sure to read the #guidelines and #faq.`"; //Check sudo cmds to see how I recognize URLS. Create variables for channels/ID's.  
    }   
     
    if (response === null && (content.indexOf("put_something_here") !== -1 || content.indexOf("put_something_here") !== -1 || /\bp+l+s+\b/g.exec(content)) ) {
         response = "put_response_here " + mentionAuthor(message) + ".";
    }         
    if (response === null && content.indexOf("hate") !== -1) {
        response = "Hatҽ is such a strong word " + mentionAuthor(message) + ". Maybe you'd like to rephrase yourself?";
    }     
    
    if (response === null && (content.indexOf("put_something_here") !== -1 || /\bp+a+r+a+\b/g.exec(content)) ) {
        i = Math.floor(Math.random() * 4); 
        if (i === 0) {
            response =  "put_response_here"; 
        }
        else if (i === 1) {
            response = "put_response_here";
        }
        else if (i === 2) {
            response = "put_response_here";            
        }
        else if (i === 3) {
            response = "put_response_here"; 
        }
    }
    
    if (response === null && content.indexOf("put_something_here") !== -1) {
        response = "put_response_here";
    }
    
    if (response !== null) {
        console.log("-----\n", "message:", message, "\nresponse:", response);
        this.sendMessage(message.channel, response);
    }
    
    // var response = mentionAuthor(message) + " " ;
    // console.log("response:", response);
    // this.sendMessage(message.channel, response);    
}

function mentionAuthor(message) {
    return message.author.mention();
    
    }

/**
 * 
 * Example Message
 * 
 * 
{ time: 1439627260790,
  author: 
   { username: 'ian',
     discriminator: '7235',
     id: '81482866575282176',
     avatar: '47be0a722500c6943f201ddd36e711a6' },
  content: 'test',
  channel: 
   { name: 'developer-channel',
     server: 
      { region: 'us-west',
        ownerID: '81448522213228544',
        name: 'LAGG',
        id: '81452874285977600',
        members: [Object],
        channels: [Object],
        icon: '0e54f0a7f444a810e07150d577ecd635',
        afkTimeout: 900,
        afkChannelId: '81455340184403968' },
     type: 'text',
     id: '81458049532821504',
     isPrivate: undefined },
  id: '82027419438940160',
  mentions: { discriminator: 'id', contents: [] },
  everyoneMentioned: false }
 * 
 */

//-----------------------------BOT-COMMANDS------------------------------

function mentionAuthor(message) {
  return message.author.mention();
}

var commands = {
  "ping!": {
    description: "Useful for checking if the bot is alive.",
    process: function(bot, message,
      msg, suffix) {
      bot.sendMessage(message.channel, 
        message.sender + " '¡ ᵖ ᵒ ᶰ ᵍ'" //why aren't mentions highlighting like usual?
      );
    }
  },
  "announce": { //doesn't work yet 
    usage: "<message>",
    description: "bot says message with text to speech",
    process: function(bot, msg,
      suffix) {
      bot.sendmessage(channel,
        "message", 1);
    }
  },
  "ubiquity": {
    description: "Send message to the channel.",
    adminOnly: true,
    process: function(bot, msg) {
      bot.sendMessage(msg.channel,
        "Still under construction..."
      );
    }
  },
  "setname": {
    description: "bot sets a name.",
    adminOnly: true,
    process: function(bot, msg,
      suffix) {
      bot.setUsername(suffix);
    }
  },
  "resetname": {
    process: function(bot, msg,
      suffix) {
      bot.setUsername("Ubiquity™");
      bot.sendMessage(msg.channel,
        "Ubiquity set to default."
      );
    }
  },
  /*"angry-ubi": {
    process: function(bot, msg,
      suffix) {
      bot.setUsername("Ubi-4000™");
      bot.sendMessage(msg.channel,
        "Ubiquity has been angered!"
      );
    }
  },*/
  "idle": { //want to include message to main channel, Parasite has gone AFK/Returned 
    description: "Sets status to idle.",
    adminOnly: true,
    process: function(bot, msg) {
      bot.setStatusIdle();
      bot.sendMessage(msg.channel,
        "*has gone idle*.");
    }
  },
  "online": {
    description: "sets bot status to online",
    adminOnly: true,
    process: function(bot, msg) {
      bot.setStatusOnline();
      bot.sendMessage(msg.channel,
        "**has come online**!");
    }
  },
  "avatar": {
    adminOnly: true,
    process: function(bot, msg,
      suffix) {
      if (msg.mentions.length === 0) {
        bot.sendMessage(msg.channel,
          "Your avatar is:\n" +
          msg.sender
          .avatarURL);
        return;
      }
      var msgArray = [];
      for (var index in msg.mentions) {
        var user = msg.mentions[
          index];
        if (user.avatarURL === null) {
          msgArray.push(user.username +
            " is naked! :o.");
        } else {
          msgArray.push(user.username +
            "'s avatar:\n" + user
            .avatarURL);
        }
      }
      bot.sendMessage(msg.channel,
        msgArray);
    }
  },
  "help": {
    description: "Send help command usage message to the channel.",
    adminOnly: true,
    process: function(bot, msg) {
      bot.sendMessage(msg.channel,
        msg.sender + 
        "`Usage: sudo cmds **suffix**`"
      );
    }
  },
  "about": {
    description: "Send about command usage message to the channel.",
    adminOnly: true,
    process: function(bot, msg) {
      bot.sendMessage(msg.channel,
        msg.sender +
        "`Usage: sudo whatami **suffix**`"
      );
    }
  },
  "cmds": {
    process: function(bot, msg) {
		if (msg.channel instanceof Discord.PMChannel) { //check if message is PM
			bot.sendMessage(msg.author, help);
		}
		else{ //if not send normal help with return to channel. 
			bot.sendMessage(msg.author, help + "`Back to: `" + msg.channel + "` channel.Â Â Â `");			
			bot.sendMessage(msg.channel, "`Please check your PM's `" + msg.sender + "`, for more information!`");
		}      
    }
  },
  "fun": {
    adminOnly: true,
    process: function(bot, msg) {
      bot.sendMessage(msg.channel,
        fun);
    }
  },
  "useful": {
    adminOnly: true,
    process: function(bot, msg) {
      bot.sendMessage(msg.channel,
        useful);
    }
  },
  "gif": {
    adminOnly: true,
    process: function(bot, msg,
      suffix) {
      var query = suffix;
      if (!query) {
        bot.sendMessage(msg.channel,
          msg.sender +
          " Usage: sudo gif **tags_here**"
        );
        return;
      }
      var tags = suffix.split(" ");
      get_gif(tags, function(id) {
        if (typeof id !==
          "undefined") {
          bot.sendMessage(msg.channel,
            "http://media.giphy.com/media/" +
            id + "/giphy.gif"
          );
        } else {
          bot.sendMessage(msg.channel,
            "I could not find anything..!: " +
            tags);
        }
      });
    }
  },
  "join-server": {
    adminOnly: true,
    process: function(bot, msg,
      suffix) {
      var query = suffix;
      if (!query) {
        bot.sendMessage(msg.channel,
          "Usage: sudo join-server **invitation link**"
        );
        return;
      }
      var invite = msg.content.split(
        " ")[1];
      bot.joinServer(invite,
        function(error, server) {
          if (error) {
            bot.sendMessage(msg.channel,
              "Assimilation complete!: " +
              error);
          } else {
            bot.sendMessage(msg.channel,
              "Assimilation unsucessful... " +
              server);
          }
        });
    }
  },
  "info": {
		process: function(bot, msg, suffix) {
			if (msg.mentions.length == 0) {
				var username = msg.author.username;
				var userID = msg.author.id;
				var discriminator = msg.author.discriminator;
				var status = 'offline' ? 'online' : 'offline'
;
				var avatar = msg.author.avatarURL;
				var userinfo = ("```Name: " + username + "\nID: " + userID + "\nDiscriminator: " + discriminator + "\nStatus: " + status + "\nAvatar: " + avatar + "```");
				bot.sendMessage(msg, userinfo);
			} else {
				for (var user of msg.mentions)
					if (user != null) {
						var username = user.username;
						var userID = user.id;
						var discriminator = user.discriminator;
						var status = 'offline' ? 'online' : 'offline';
						var avatar = user.avatarURL;
						var userinfo = ("```Name: " + username + "\nID: " + userID + "\nDiscriminator: " + discriminator + "\nStatus: " + status + "\nAvatar: " + avatar + "```");
						bot.sendMessage(msg, userinfo);
					}
			}
	}
  },
  "whoami": {
	description: "Send user ID to the channel.",
    adminOnly: false,
    process: function(bot, msg) {
      bot.sendMessage(msg.channel,
        msg.author.id);
    }
  },
  "whatami": {
    description: "Send bot 'About' message to the channel.",
    adminOnly: false,
    process: function(bot, msg) {
      bot.sendMessage(msg.channel,
        msg.sender +
        " Under Construction");
    }
  },
  "whereami": { //information won't display correctly 
    description: "Send bot location information to the channel.",
    adminOnly: false,
    process: function(bot, msg,
      message) {
      bot.sendMessage(msg.channel,
        msg.sender +
        " You are located in " +
        "Channel_Name_here " +
        "Channel_ID_here " + "\n" +
        "On server " +
        "Server_Name_here " +
        "Server_ID_here " +
        "Server_Region_here " +
        "\n" + "Owned by: " +
        "Server_Owner_here " +
        "Server_Owner_ID_here " +
        "\n" +
        "The channel description is: " +
        "Channel_Topic_here " +
        "bot name " + bot.user +
        "connected to " +
        bot.users.length +
        " people");
    }
  },
  "quote": {
    adminOnly: true,
    process: function(bot, msg) {
      var rand = Math.floor(Math.random() *
        Quotes.length);
      bot.sendMessage(msg.channel,
        Quotes[rand]);
    }
  },
  "coin": {
		process: function(bot, msg) {
			var number = Math.floor(Math.random() * 2) + 1;
			if (number === 1) {
				bot.sendFile(msg.channel, "./images/Heads.png");
			} else {
				bot.sendFile(msg.channel, "./images/Tails.png");
			}
		}
  },
  "roll": {
    process: function(bot, msg,
      suffix) {
      if (suffix) {
        var number = Math.floor(
            Math.random() * suffix) +
          1;
      } else {
        var number = Math.floor(
          Math.random() * 6) + 1;
      }
      bot.sendMessage(msg.channel,
        msg.sender + " Rolled " + 
          number);
    }
  },
  "servers": {
    adminOnly: true,
    process: function(bot, msg) {
      var list = bot.servers.sort();
      bot.sendMessage(msg.channel,
        list);
    }
  },
  "snoopify": {
    adminOnly: true,
    process: function(bot, msg,
      suffix) {
      var query = suffix;
      if (!query) {
        bot.sendMessage(msg.channel,
          "Usage: sudo snoopify **sentence**"
        );
        return;
      }
      var G = require('gizoogle');
      G.string(suffix, function(
        error, translation) {
        bot.sendMessage(msg.channel,
          translation);
      });
    }
  },
  "uptime": {
    adminOnly: true,
    process: function(bot, msg) {
      var uptimeh = Math.floor((bot
        .uptime / 1000) / (60 *
        60));
      var uptimem = Math.floor((bot
          .uptime / 1000) % (60 *
          60) /
        60);
      var uptimes = Math.floor((bot
        .uptime / 1000) % 60);
      bot.sendMessage(msg.channel,
        "I have been connected to Discord for:\n" +
        uptimeh +
        " Hours\n" +
        uptimem + " Minutes\n" +
        uptimes + " Seconds\n");
    }
  },
  "urban": {
    adminOnly: true,
    process: function(bot, msg,
      suffix) {
      var query = suffix;
      if (!query) {
        bot.sendMessage(msg.channel,
          "Usage: sudo urban **search terms**"
        );
        return;
      }
      var Urban = require('urban');
      Urban(suffix).first(function(
        json) {
        if (json !== undefined) {
          var definition = "" +
            json.word + ": " +
            json.definition +
            "\n:arrow_up: " +
            json.thumbs_up +
            "   :arrow_down: " +
            json.thumbs_down +
            "\n\nExample: " +
            json.example;
          bot.sendMessage(msg.channel,
            definition);
        } else {
          bot.sendMessage(msg.channel,
            "Sorry, I couldn't find a definition for: " +
            suffix);
        }
      });
    }
  },
  "wiki": {
    adminOnly: true,
    process: function(bot, msg,
      suffix) {
      var query = suffix;
      if (!query) {
        bot.sendMessage(msg.channel,
          "Usage: sudo wiki **search terms**"
        );
        return;
      }
      var Wiki = require('wikijs');
      new Wiki().search(query, 1).then(
        function(data) {
          new Wiki().page(data.results[
            0]).then(function(
            page) {
            page.summary().then(
              function(
                summary) {
                var sumText =
                  summary.toString()
                  .split(
                    '\n');
                var
                  continuation =
                  function() {
                    var
                      paragraph =
                      sumText
                      .shift();
                    if (
                      paragraph
                    ) {
                      bot.sendMessage(
                        msg
                        .channel,
                        paragraph,
                        continuation
                      );
                    }
                  };
                continuation
                  ();
              });
          });
        });
    }
  },
  "youtube": {
    adminOnly: true,
    process: function(bot, msg,
      suffix) {
      var query = suffix;
      if (!query) {
        bot.sendMessage(msg.channel,
          "Usage: sudo youtube **video tags**"
        );
        return;
      }
      var yt = require(
        "./youtube_plugin");
      var youtube_plugin = new yt();
      youtube_plugin.respond(suffix,
        msg.channel, bot);
    }
  },
  "kill": {
    description: "Kills all running instances of Ubiquity.",
    adminOnly: true,
    process: function(bot, msg) {
        bot.sendMessage(msg.channel, "Whyyyyyy..!?"); //doesn't send message to channel first, why? 
        console.log("Ubiquity terminated!");
        process.exit(1); //exit node.js with an error
    } 
  },
  "log": {
    usage: "<log message>",
    description: "logs message to console",
    adminOnly: true,
    process: function(bot, msg, suffix) {
		console.log(msg.content);
    }
  },
};
  
//-----------------------------BOT-COMMANDS------------------------------END

var help = multiline(function() { 
  /*
  ```py
'Ubiquity' Commands Menu:

  s̲u̲d̲o̲_̲f̲u̲n̲
      'coin' | 'quote' | 'roll' | 'gangstify'

  s̲u̲d̲o̲_̲u̲s̲e̲f̲u̲l̲
      'avatar' | 'gif' | 'image' | 'join-server' | 'prune' | 'urban' | 'wiki' | 'youtube'

   ̲s̲u̲d̲o̲_̲i̲n̲f̲o̲
      'whoami' | 'whereami' | 'whatami' | 'log' | 'servers' | 'uptime'
  ```
  */
});

var fun = multiline(function() {
  /*
  ```js
'Fun' Commands Menu: 

  sudo quote
        Generate a 'random quote'.
  	  
  sudo roll 'number'
        Roll a 'six-sided die', or 'choose sides'.
  ```
  */
});

var useful = multiline(function() {
  /*
  ```py
  
'Useful' Commands Menu: 
  
  sudo avatar '@Username'
        Responds with your 'Avatar', or @mentioned users.

  sudo gif 'gif tags'
        Gets a gif from 'Giphy' matching the given tags.

  sudo join-server 'invite link'
        Will join the 'server' invited to.

  sudo urban 'search_terms'
        Returns the summary of the first matching search result from 'Urban Dictionary'. 

  sudo wiki 'search_terms'
        Returns the summary of the first matching search result from 'Wikipedia'. 

  sudo youtube 'video tags'
        Gets a video from 'Youtube' matching the given tags. 
  ```
  */
});

var info = multiline(function() { // ( ʹ ) is not recognized in highlighted output. 
  /*
  ```js
'Information' Commands Menu:

  sudo whoami
        Responds with the 'user ID' of the sender.
		
  sudo whatami
        Responds with Ubiquityʹs 'porpoise'.  

  sudo whereami
         Responds with Ubiquityʹs 'location'.

  sudo servers
        Lists all the 'servers' Ubiquity is connected to.

  sudo uptime
        Shows how long Ubiquity has been 'operational'.
  ```
  */
});

var Quotes = [
  "Going to church doesn’t make you a Christian any more than standing in a garage makes you a car. - *Billy Sunday*",
  "I dream of a better tomorrow, where chickens can cross the road and not be questioned about their motives.",
  "The grass may be greener on the other side, but it's still grass! - *Scott M. Ligocki Jr.*",
  "As I understood more, I came to realize that I know nothing, and before I start to change others, I should start with myself. - *Vivek Subramaniam*",
  "I asked God for a bike, but I know God doesn’t work that way. So I stole a bike and asked for forgiveness. - *Emo Philips*",
  "The illiterate of the 21st century will not be those who cannot read and write, but those who cannot learn, unlearn, and relearn. - *Alvin Toffler, American Futurist and Writer*",
  "The Net interprets censorship as damage and routes around it. - *John Gilmore, Co-founder of Electronic Frontier Foundation (EFF)*",
  "The trouble with most people is that they think with their hopes or fears or wishes rather than with their minds. - Will Durant*",
  "Learn how to see. Realize that everything connects to everything else. - Leonardo da Vinci*",
  "I dream of a better tomorrow, where chickens can cross the road and not be questioned about their motives.",
  "When tempted to fight fire with fire, remember that the Fire Department usually uses water.",
  "My favorite machine at the gym is the vending machine.",
  "I always arrive late at the office, but I make up for it by leaving early. - *Charles Lamb*",
  "Just do it. - *Shia Labeouf*",
  "Don't let your dreams be memes. - *Shia LaBeouf*",
  "Jet fuel can't melt steel beams. - *Barack Obama*",
  "Jet fuel can't melt steel memes. - *Barack Obama*",
  "Born too late to explore the earth\nBorn too soon to explore the galaxy\nBorn just in time to **browse the Interwebz**",
  "Feels good man. - *Pepe*",
  "Press F to pay respects.",
  "We don't make mistakes, just happy little accidents. - *Bob Ross*",
  "There's nothing wrong with having a tree as a friend. - *Bob Ross*",
  "The only thing worse than yellow snow is green snow. - *Bob Ross*",
  "Shwooop. Hehe. You have to make those little noises, or it just doesn't work. - *Bob Ross*",
  "I like to beat the brush. - *Bob Ross*",
  "Just tap it. - *Bob Ross*",
  "That'll be our little secret. - *Bob Ross*",
  "The secret of happiness, you see, is not found in seeking more, but in developing the capacity to enjoy less. - *Socrates*",
  "Two hands working can do more than a thousand clasped in prayer. - *Unknown",
  "Christianity began as a personal relationship with Jesus Christ. When it went to Athens, it became a philosophy. When it went to Rome, it became an organization. When it went to Europe, it became a culture. When it came to America, it became a business. - *Unknown*",
  "People think being alone makes you lonely, but I don't think that's true. Being surrounded by the wrong people is the loneliest thing in the world. - *Kim Culbertson*",
];

function get_gif(tags, func) {
  //limit=1 will only return 1 gif
  var params = {
    "api_key": "dc6zaTOxFJmzC",
    "rating": "r",
    "format": "json",
    "limit": 1
  };
  var query = qs.stringify(params);
  if (tags !== null) {
    query += "&q=" + tags.join('+');
  }

  var request = require("request");

  request(
    "http://api.giphy.com/v1/gifs/search" +
    "?" + query,
    function(error,
      response, body) {
      var responseObj = JSON.parse(
        body);
      if (responseObj.data.length) {
        func(responseObj.data[0].id);
      } else {
        func(undefined);
      }
    }.bind(this));
}

//New Bot Restart on Disconnect Code, see if it works...

bot.on("disconnected", function() {
	console.log(currentTime() + "Disconnected. Ubiquity attempting to reconnect...");
	sleep(5000);
	bot.connect()
	bot.login(Config.email, Config.password);
});


//isn't working: https://github.com/foreverjs/forever-monitor to restart bot on disconnect 
//Don't use forever. Use PM2. Process Manager 2. Instead of node program.js. use pm2 start program.js 

bot.login(Config.email, Config.password);